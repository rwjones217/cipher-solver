import random

# How many elite are we going to keep from the previous generation
ELITE_NUM = 10
# Probability that the best score wins
TOURN_PROBABILITY = .5

"""
population: current population
size: the size of each tournament
returns the winner from the tournament
"""
def tournament(population: list, size: int) -> list:
    winners = []
    competitors = []

    # Set up the list of probabilities for each competitor in the tournament
    # for index i, where the competitor with the lowest index has the highest score
    # p = TOURN_PROBABILTY * ((TOURN_PROBABILTY - 1)^i)
    probabilty_list = []
    for i in range(size):
        probabilty_list.append((1 - (TOURN_PROBABILITY * ((1 - TOURN_PROBABILITY) ** i))) *10000)



    # Size of individual tournaments
    tournament_size = len(population)//size

    current_population_length = len(population)

    while (current_population_length >= size):
        # Get competitors 
        for i in range(size):
            next_competitor_index = random.randint(0, len(population) - 1)
            competitors.append(population.pop(next_competitor_index))
        # sort competitors by score
        competitors.sort(key=lambda individual: individual.score, reverse=True)

        # Select winner
        r = random.randint(0, 10000)
        winning_index = 0
        for p in range(len(probabilty_list)):
            if r <= probabilty_list[p]:
                winning_index = p
        current_population_length = len(population)
        winners.append(competitors[winning_index])

    #TODO: Do a tournament with the remainders


    return winners



        

            
