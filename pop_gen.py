import random
from solution import Solution
from ciphertext import CipherText

filename = "stage3.txt"
characters = []
patterns_2 = {}
#sorted_patterns = {}



def add_to_characters(char, lyst):
    if not (char in lyst):
        lyst.append(char)

def print_sorted_values(d):
    for i in d:
        print("{}: {}".format(i, d[i]))


def count():
    """
    open the filename and add all the characters to the characters list
    """
    try:
        with open(filename, 'r') as f:
            data = f.read().replace('\n','')
            split_text = [data[i:i+2] for i in range(0, len(data), 2)]
            for i in split_text:
                add_to_characters(i, characters)
    except Exception as e:
        print(e)



def generate_solution(key, alphabet="abcdefghijklmnopqrstuvwxyz", random_object=None) -> Solution:
    '''
    Generates a single representation of a solution
    '''
    #Random object
    if random_object:
        r = random_object
    else:
        r = random.Random()

    count()
    alphabet = "abcdefghijklmnopqrstuvwxyz"
    sol = []
    # Create an entry for each character
    for c in characters:
        sol.append(r.choice(alphabet))
    solution = Solution(sol.copy(), key)
    return solution

def generate_population(key: list, n: int = 20, random_object=None) -> list:
    """
    Create a list of solutions
    Arguments:
    - key: list of symbols
    - n: number of solutions to create
    
    """
    if random_object:
        args = (key, "abcdefghijklmnopqrstuvwxyz", random_object)
    else:
        args = (key, "abcdefghijklmnopqrstuvwxyz", None)
    population = []
    for i in range(n):
        population.append(generate_solution(args[0], args[1], args[2]))
    return population


def main():
    ct = CipherText()
    ct.load_json("cipher.json")
    key = ct.get_key()
    pop = generate_population(key)
    for i in pop:
        print(i)
    sorted_pop = sorted(pop, key = lambda sol: sol.score)

if __name__ == '__main__':
    main()