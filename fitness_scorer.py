
import pop_gen
import replace_letters
from solution import Solution

def score_word(word: str):
    """
    Function for determining the score of a word that will be used for the GA's fitness score
    """
    # If the word is two letters long or shorter, we do not count it
    if len(word) <= 2:
        return 0
    elif len(word) >=5:
        return len(word) * 1.5

    return len(word)

def score_ioc(text):
    # Remove non-alphabetic characters and convert to uppercase
    filtered_text = ''.join(filter(str.isalpha, text)).upper()
    N = len(filtered_text)

    if N <= 1:
        return 0  # Avoid division by zero or negative values

    # Calculate the frequency of each letter
    frequencies = {}
    for letter in filtered_text:
        frequencies[letter] = frequencies.get(letter, 0) + 1

    # Sum the products of each frequency with its frequency minus one
    sum_of_products = sum(f * (f - 1) for f in frequencies.values())

    # Calculate the Index of Coincidence
    IC = sum_of_products / (N * (N - 1))

    return IC

def create_word_scores(wordlist: list):
    """
    Go through the word list and return the dictionary scores
    """
    scores = {}
    for word in wordlist:
        score = score_word(word)
        scores[word] = score
    return scores

def score_solution(word_scores: dict, text: str, line_len: int = 10):
    """
    Loops through the text and determines whether or not there is a word from the list in a "line"
    """
    sol_score = 0
    found_words = []
    try:
        for word in word_scores:
            for char_index in range(line_len, len(text), line_len):
                found = False
                if word in text[(char_index - line_len):char_index]:
                    found = True
                    sol_score += word_scores[word]
                    found_words.append(word)
                    #print("Found: {} Text: {}".format(word, text[(char_index - line_len):char_index]))

                # We will go past the end if we continue. Add the last section and break
                if char_index + line_len > len(text):
                    if word in text[char_index:len(text)]:
                        sol_score += word_scores[word]
                        found_words.append(word)
                    break
    except Exception as e:
        print(e)
        return None

    return sol_score, found_words

# TODO change threshold to upper and lower limit
def vowel_threshold(sol: dict, symbol_dict: dict, threshold: int = 200) -> bool:
    """
    Check if the number of vowels in the text with this solution meets a minimum threshold
    """
    vowels = {}
    for i in sol.keys():
        if sol[i] in "aeiouAEIOU":
            vowels[i] = 1
        else:
            vowels[i] = 0
    # Multiply by vowel occurrences
    vowels = {i:vowels[i] * symbol_dict[i] for i in vowels.keys()}
    total_vowels = sum(vowels.values())
    if total_vowels > threshold:
        return True
    else:
        return False

def main():
    try:
        # Dictionary of words with their scores
        ws = {}
        symbol_count = 0
        with open("basic_english_2000.txt") as f:
            words = [line.rstrip() for line in f]
            ws = create_word_scores(words)
        replacements = pop_gen.generate_solution()
        # List of solutions
        population = pop_gen.generate_population(20)
        text = replace_letters.read_file("stage3.txt")
        symbols_dict = replace_letters.create_pair_dict(text)
        symbol_count = sum(symbols_dict.values())
        vt = int(.2 * symbol_count)

        for solution in population:
            replaced_text = replace_letters.replace_text("stage3.txt", solution.get_solution())
            if vowel_threshold(solution.get_solution(), symbols_dict, vt):
                score, found_words = score_solution(ws, replaced_text)
                solution.update_score(score)
            else:
                solution.update_score(0)
        for i in population:
            print(i)
    except Exception as e:
        print("An Error Occurred: {}".format(e))

if __name__ == '__main__':
    main()


