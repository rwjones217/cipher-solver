#!./env/bin/python
from ciphertext import CipherText
import fitness_scorer as fs
import pop_gen
import os
import replace_letters
import copy
import selection
import random

WORDS_FILE = "basic_english_2000.txt"
CIPHER_JSON = "cipher.json"
CIPHER_FILE = "stage3.txt"

MAX_GENERATIONS = 20
SUCCESS_SCORE = 200
ENGLISH_IOC = 0.0667

"""
Takes a population of solutions and a key...
params: population, key
"""
def run_cycle(population: list, key: list) -> bool:
    ## Selection
    pop_size = len(population)
    # Take out the ELITE
    population.sort(key=lambda individual: individual.score, reverse=True)

    #
    elites = []
    for i in range(2):
        elites.append(population.pop(0))

    # list of tournament winners
    tournament_winners = selection.tournament(population, 10)
    mutation_pool = elites + tournament_winners
    
    population.clear()

    # Keep the top X elites as is
    population = population + elites


    #TODO: Add another selection method
    # Tournament Selection

    # Create a new population from the mutation pool
    while len(population) < pop_size:
        # Randomly select an individual from the mutation pool
        index = random.randint(0, len(mutation_pool)-1)
        # Copy the selected individual for mutating
        individual = copy.deepcopy(mutation_pool[index])
        # Mutate the copy
        individual.mutate_solution(10, key)
        #mutation_pool[index].mutate_solution(10, key)
        # Add it to the population
        population.append(individual)



    return population

def score_population(pop: list, cipherText: CipherText) -> list:
    """
    Takes the population and updates the score of each solution in the population

    Arguments:
    - pop: list of solution objects
    - cipherText: CipherText object containing the in question ciphertext

    Returns the updated population
    """
    with open(WORDS_FILE) as f:
        words = [line.rstrip() for line in f]
        word_scores = fs.create_word_scores(words)

    # Vowel Threshold. Completely arbitrary right now
    vt = int(.2 * cipherText.symbol_count())

    for s in pop:
        replaced_text = replace_letters.replace_text(CIPHER_FILE, s.get_solution())
        if fs.vowel_threshold(s.get_solution(), cipherText.symbols, vt):
            #score, found_words = fs.score_solution(word_scores, replaced_text, 20)
            score = - abs(fs.score_ioc(replaced_text) - ENGLISH_IOC)
            s.update_score(score)
        else:
            s.update_score(-1000)


def main():
    """
    Main function
    """
    # Set up the Cipher text object
    ct = CipherText()
    
    with open(WORDS_FILE) as f:
        words = [line.rstrip() for line in f]
        word_scores = fs.create_word_scores(words)
    if os.path.exists(CIPHER_JSON):
        ct.load_json(CIPHER_JSON)
        print("{} Loaded from json".format(ct))
    else:
        ct = CipherText(CIPHER_FILE)
        print("{} new ct".format(ct))
        ct.save_json(CIPHER_JSON)
    key = ct.get_key()
    
    # Vowel Threshold. Completely arbitrary right now
    # We use this to check if a solution has a reasonable amount of vowels 
    # to be a real phrase.
    vt = int(.2 * ct.symbol_count())

    # Generate a population of potential solutions to start with.
    population = pop_gen.generate_population(key, 100)
    # Score each solution
    print("######INITIAL GENERATION######")
    for s in population:
        replaced_text = replace_letters.replace_text(CIPHER_FILE, s.get_solution())
        if fs.vowel_threshold(s.get_solution(), ct.symbols, vt):
            #score, found_words = fs.score_solution(word_scores, replaced_text, 20)
            ioc = fs.score_ioc(replaced_text)
            score = -abs(ioc - ENGLISH_IOC)
            s.update_score(score)
        else:
            s.update_score(-1000)
    for i in population:
        print(f"Score: {i.score}")

    #print(f'IOC Ciphertext: {fs.calculate_index_of_coincidence(ct.cipherText)}')
    #print(f'Complete test: {fs.calculate_index_of_coincidence("This is a test of IOC. Nothing else.")}')

    best_solution = None
    # Loop through the max generations
    for run in range(MAX_GENERATIONS):
        print("#####RUN NUMBER {}#####".format(run))
        population = run_cycle(population, key)
        high_score = -1000
        # TODO Need to rescore the new population
        for s in population:
            replaced_text = replace_letters.replace_text(CIPHER_FILE, s.get_solution())
            if fs.vowel_threshold(s.get_solution(), ct.symbols, vt):
                #score, found_words = fs.score_solution(word_scores, replaced_text, 20)
                score = - abs(fs.score_ioc(replaced_text) - ENGLISH_IOC)

                if score > high_score:
                    high_score = score
                    best_solution = s
                s.update_score(score)
            else:
                s.update_score(-1000)
        print("High score: {}".format(high_score))

    print(best_solution)
    print(replace_letters.replace_text(CIPHER_FILE, best_solution.get_solution()))



    
    




if __name__ == '__main__':
    main()
