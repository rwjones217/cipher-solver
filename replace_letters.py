import pop_gen


def read_file(file_path):
    with open(file_path, 'r') as f:
        return f.read().replace('\n', '')

def create_pair_dict(text):
    pair_dict = {}
    for i in range(0, len(text) - 1, 2):
        pair = text[i:i+2]
        if pair not in pair_dict:
            pair_dict[pair] = 0
        pair_dict[pair] += 1
    return pair_dict

def count_symbols(text):
    symbols = create_pair_dict(text)
    return (sum(symbols.values()))

def replace_pairs(text, replacements):
    new_text = []
    i = 0
    while i < len(text):
        pair = text[i:i + 2]  # Extract the current pair from the text
        new_char = replacements.get(pair, pair)  # Replace the pair if it exists in the replacements dictionary
        new_text.append(new_char)
        i += 2  # Move on to the next pair
    return ''.join(new_text)



def insert_newlines(text, line_length=55, enabled=False):
    if enabled:
        return '\n'.join([text[i:i+line_length] for i in range(0, len(text), line_length)])
    else:
        return text
    
def write_file(file_path, text):
    with open(file_path, 'w') as f:
        f.write(text)

def replace_text(file_path, replacements):
    text = read_file(file_path)

    # Create a dictionary of every pair of characters
    #pair_dict = create_pair_dict(text)

    # Define replacements
    # Replace 'th' with 'Z', 'an' with 'Y' for example
    #replacements = pop_gen.generate_solution()

    # Replace the pairs with single characters
    new_text = replace_pairs(text, replacements)

    # Insert newlines every 30 characters
    formatted_text = insert_newlines(new_text)

    return formatted_text

