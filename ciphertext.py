import json
ALPHABET = "abcdefghijklmnopqrstuvwxyz"

class CipherText:
    """
    This class will contain the ciphertext, symbols, and the alphabet we will be using to decipher
    """

    cipherText = None
    symbols = {}
    # Symbols key is just the list of symbols that will be matched up to every "solution"
    symbols_key = []

    def __init__(self, filepath: str = None):
        if (filepath):
            self.load_file(filepath)
            try:
                self.load_symbols()
            except Exception as e:
                print(e)

    def __str__(self):
        return "Ciphertext: {}\nSymbols: {}".format(self.cipherText, self.symbols)

    def load_file(self, file_path: str) -> None:
        with open(file_path, 'r') as f:
            self.cipherText = f.read().replace('\n', '')

    def load_symbols(self):
        pair_dict = {}
        for i in range(0, len(self.cipherText) - 1, 2):
            pair = self.cipherText[i:i+2]
            # Load it into the symbol dictionary, and also the symbol list (If we haven't seen it)
            if pair not in pair_dict:
                pair_dict[pair] = 0
                self.symbols_key.append(pair)
            pair_dict[pair] += 1
        self.symbols = pair_dict
        self.set_key()

    def symbol_count(self):
        return len(self.symbols)

    def save_json(self, filename: str) -> None:
        """
        Save the ciphertext and symbols as a json file to view and load for future use
        """
        data = {
            'ciphertext': self.cipherText,
            'alphabet': ALPHABET,
            'symbols': self.symbols
        }
        with open(filename, 'w') as f:
            json.dump(data, f)

    def load_json(self, file_path:str) -> None:
        """
        Load a json representation of the ciphertext and symbols
        """
        with open(file_path, 'r') as f:
            data = json.load(f)
        self.cipherText = data['ciphertext']
        self.symbols = data['symbols']
        self.set_key()

    def get_key(self) -> list:
        """
        Returns the key for the solution
        """
        return self.symbols_key
    
    def set_key(self) -> None:
        self.symbols_key = list(self.symbols.keys())

def main():
    c = CipherText('stage3.txt')
    c.save_json('cipherInfo.json')

    new_c = CipherText()
    new_c.load_json('cipherInfo.json')
    print(new_c)
    

if __name__ == '__main__':
    main()
