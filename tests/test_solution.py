import pytest
import sys
sys.path.append("..")
from solution import Solution
from random import Random



def test_mutate():
    # Set a seed value for repeatability
    random = Random(666)
    alphabet = 'abcdef'
    key = [0, 1, 2, 3, 4, 5]
    s = Solution(['a','b','c','d','e','f'], key, random)
    s.mutate_solution(3, key)

    assert s.solution == ['a','b','c','m','n','j']