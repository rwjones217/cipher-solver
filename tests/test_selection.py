#import pytest
import sys
sys.path.append("..")
from solution import Solution
from random import Random
from ciphertext import CipherText
import pop_gen
import selection
import os

WORDS_FILE = "basic_english_2000.txt"
CIPHER_JSON = "cipher.json"
CIPHER_FILE = "stage3.txt"

key = None
population = None

def setup():
    r = Random(666)
    ct = CipherText()
    with open(WORDS_FILE) as f:
        words = [line.rstrip() for line in f]
        word_scores = fs.create_word_scores(words)
    if os.path.exists(CIPHER_JSON):
        ct.load_json(CIPHER_JSON)
    else:
        print("Cipher Json file: {} does not exist.".format(CIPHER_JSON))
    key = ct.get_key()

    population = pop_gen.generate_population(key, 10, r)
    for i in population:
        print(i)

def main():
    setup()

if __name__ == '__main__':
    main()





    



def test_tournament():
    setup()