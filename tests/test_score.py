import pytest
import sys
sys.path.append("..")
import fitness_scorer

wordlist = ['a', 'the', 'help']
scores  = {'a': 1, 'the': 3, 'help':4}
example_text = 'nladifubithesfulibhasevauhelpdhglih'

def test_create_word_scores():
    """

    """
    assert fitness_scorer.create_word_scores(wordlist) == scores
    assert fitness_scorer.create_word_scores([]) == {}

def test_score_solution():
    score_tuple = fitness_scorer.score_solution(scores, example_text, 15)
    assert score_tuple[0] == 9
    assert score_tuple[1] == ['a', 'a', 'the', 'help']