# cipher-solver



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## The Problem
At the end of Simon Singh's book *The Code Book*, he presents several challenges based on the ciphers and codes he explains throughout the book. The "Stage 3" challenge is titled "Monoalphabetic Cipher with Homophones". It contains a solid (no spaces) block of text of capital letters and asterisks (*). 

## The Genetic Algorithm



### Setting up a virtual environment
In a terminal window, navigate to this project directory.

Check that you have virtualenv installed with the command:
`virtualenv --version`

If not, run the command: `sudo pip install virtualenv`

Once virtualenv is installed, run the command `python -m venv env` inside your project directory.

This should create a new directory called `env`

Run the command: `source env/bin/activate` to activate your virtual environment.

This allows you to run tests with: `python -m pytest`

## Name
Choose a self-explaining name for your project.

## Description
A collection of python scripts for solving a monoalphabetic cipher with homophones using genetic algorithms.

### Solution Class
`solution.py` contains the Solution class that houses the dictionary of symbols and corresponding letter.

Variables in the class include:\
```sol``` is a list of letters that represents the solution when lined up with a ```key``` which is a persistent list of symbols from the ciphertext.

```replacement_solution``` This is updated everytime `sol` changes so `replace_letters.py` can easily decrypt the ciphertext with the solution.

`vowel_threshold` a bool, if `True` then the vowel threshold was passed, so we will continue to score this solution. If `False` the score will remain zero and this solution will be tossed out in the selection phase of `main.py`.

#### Methods

### Scoring
In the method `fitness_scorer.py` we have a few methods for evaluating the fitness of each solution. Right now, both of them are bad. 

One invovles taking a word list and an interval of letters in the "plaintext" of the deciphering attempt.


### CipherText Class
`ciphertext.py` contains the CipherText class that contains some methods. 


## Usage
For the love of God, don't use this project.

The goal is for `main.py` to do all of the work, but there are several files to break up the different parts of the genetic algorithm. It's not there yet, but right now if you run `main.py`, nothing will happen. You might even get an error.



## Authors and acknowledgment
Will "Really Dumb Guy" Murphy<br>
Rob Jones


