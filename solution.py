
import random
ALPHABET = "abcdefghijklmnopqrstuvwxyz"
CIPHER = {}

class Solution:
    """
    This class contains all the information needed for a solution
    for the cipher text problem to be evaluated, scored, and mutated
    """

    # This dictionary is updated everytime the solution changes
    replacement_solution = {} # Dictionary that is generated for deciphering the ciphertext with the solution
    solution = []
    score = 0
    vowel_threshold = False
    random_object = random.Random()

    def __init__(self, sol: list, key: list, random_object=None) -> None:
        """
        Instantiates the solution class

        Arguments:
        - sol: list of letters that correspond to symbols in the key
        - key: list of symbols that is persistent across each solution
        - random_object: Used for testing, None by default
        """
        # If an alphabet string and symbol dictionary are provided, create the solution?
        self.solution = sol
        self.update_replacement_solution(key)
        if random_object:
            self.random_object=random_object

    def __str__(self) -> str:
        return f"Solution: {self.solution}\nScore: {self.score}"
    

    def update_score(self, score: int) -> None:
        """
        Updates the score the solution to the provided value

        Arguments:
        - score: New score for the solution

        Returns None
        """
        self.score = score

    def update_replacement_solution(self, key: list) -> None:
        """
        Updates the replacement solution dictionary using the ciphertext 'key' of symbols
        that correspondsto the solution list.

        Arguments:
        - key: list of symbols 

        Returns:
        None
        """

        for i in range(len(key)):
            self.replacement_solution[key[i]] = self.solution[i]
        self.replacement_solution = self.replacement_solution.copy()


    def get_solution(self) -> dict:
        """
        Returns the solution pairs

        Arguments:
        None

        Returns dictionary representation of the solution
        """
        return self.replacement_solution
    
    def get_score(self) -> int:
        """
        Returns the current score of the solution

        Arguments:
        None

        Returns int of the score
        """
        return self.score
    
    def get_vowel_threshold(self) -> bool:
        """
        Method to return whether or not this solution passed the vowel threshold

        Arguments:
        None

        Returns: bool True if passed, False if failed
        """
        return self.vowel_threshold
    
    def mutate_solution(self, r: int, key: list) -> None:
        """
        Mutates the solution by selecting a subsection of the solution to randomly modify

        Arguments:
        - r: an int representing the size of the range which to mutate
        - key: the symbols key list for the solution
        """
        # Pick a random number in the range of the length of the solution
        mutate_index = self.random_object.randint(0, len(self.solution) - r)

        # Create mutated section
        mutated_section = []
        for i in range(r):
            mutated_section.append(self.random_object.choice(ALPHABET))

        self.solution[mutate_index:mutate_index+r] = mutated_section        

        # Make sure the dictionary of the solution is up-to-date
        self.update_replacement_solution(key)

def main():
    sol = ['a', 'b', 'c', 'd', 'e', 'f']
    key = [0, 1, 2, 3, 4, 5]
    solution = Solution(sol, key)
    print(solution)
    solution.mutate_solution(3, key)
    print(solution)

if __name__ == '__main__':
    main()